#!/bin/sh
#
# TheFrenchGhosty's GOG Archivist Scripts: The ultimate collection of scripts to download everything you bought on GOG.com
# https://gitlab.com/TheFrenchGhosty/TheFrenchGhostys-GOG-Archivist-Scripts
# https://gitlab.com/TheFrenchGhosty
#
#

lgogdownloader --check-orphans
